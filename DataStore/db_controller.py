import os
import sqlite3


class DbController:
    def __init__(self):
        self.db_file = os.path.join(os.getcwd(), "DataStore", "app_db.sqlite")
        self.db = sqlite3.connect(self.db_file)
        self.cursor = self.db.cursor()

    def __del__(self):
        self.db.close()

    def select_matching_user(self, first_name, surname):
        self.cursor.execute(
            """
                SELECT firstName, surname, age FROM User
                WHERE firstName LIKE :first_name
                OR surname LIKE :surname
            """,
            {'first_name': f"%{first_name}%", 'surname': f"%{surname}%"}
        )
        questions_list = self.cursor.fetchall()
        return questions_list

    def add_user(self, first_name, surname, age):
        self.cursor.execute(
            """
                INSERT INTO User (firstName, surname, age)
                VALUES (:first_name, :surname, :age)
            """,
            {'first_name': first_name, 'surname': surname, "age": age}
        )
        self.db.commit()

    def authorised_user(self, username, password):
        self.cursor.execute(
            """
                SELECT COUNT(username) FROM Auth
                WHERE username =  :username
                AND password = :password
            """,
            {'username': username, 'password': password}
        )
        count = self.cursor.fetchone()
        return count[0]
